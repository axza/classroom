import React from 'react';

import { Card } from 'semantic-ui-react'

const subjectNames = {
  "subject": "Schulfach",
  "class": "Klassenstufe",
  "audience": "Zielgruppe",
  "cost": "Kosten",
  "complexity": "Komplexität (1-10)",
  "asynch": "Online-Koordination",
  "clients": "Geräte-Kompatibilität",
  "infrastructure": "Infrastruktur (Server, etc)",
  "corona_offer": "Speziell Corona"
}

const renderValueString = {
  "cost": (p) => {
    let msg = "";
    if(p.cost === "free") {
      msg += "kostenlos";
    } else if(p.cost === "paid") {
      msg += "kostenpflichtig";
    }
    if(p.cost_comment) {
      msg = msg ? msg + " (" + p.cost_comment + ")": p.cost_comment;
    }
    return msg
  },
  "clients": p => p.clients.join(', '),
  "subject": p => p.subject.join(', '),
  "audience": p => p.audience.join(', ')
}


export default class ProductCard extends React.Component {
  render() {
    const p = this.props.product;

    const info_topics = ["subject", "class", "audience", "asynch", "complexity", "cost",
      "clients", "infrastructure", "corona_offer"]
      .filter(topic => p[topic])
      .filter(topic => topic !== "clients" || p.clients.length)
      .filter(topic => topic !== "subject" || p.subject.length)
      .filter(topic => topic !== "audience" || p.audience.length)

    return (
              <Card>
                <div className="content">
              {p.logo ? <img className="right floated mini ui image" alt="Logo fehlt" src={p.logo} loading="lazy"/> : null}
                  <div className="header">{p.name}</div>
                  <div className="meta">{p.category.join(', ')}</div>
                  <div className="description">{p.short_description}</div>

                  {(p.url || p.wiki || p.test_instance) && (<div className="productCard-subSection">
                    <h4 className="ui sub header">Links</h4>
                    <table className="ui very basic collapsing celled table">
                      <tbody>
                        {p.url &&
                        (<tr>
                          <td>
                            <b>Website</b>
                          </td>
                          <td className="table-data-cell">
                            <a href={p.url}>{p.url}</a>
                          </td>
                        </tr>)
                        }{p.wiki &&
                        (<tr>
                          <td>
                            <b>Wikipedia</b>
                          </td>
                          <td>
                            <a className="table-data-cell" href={p.wiki}>{p.wiki}</a>
                          </td>
                        </tr>)
                        }{p.test_instance &&
                        (<tr>
                          <td>
                            <b>Demo-Instanz</b>
                          </td>
                          <td>
                            <a className="table-data-cell" href={p.test_instance}>{p.test_instance}</a>
                          </td>
                        </tr>)
                        }
                      </tbody>
                    </table>
                  </div>)}

                  {info_topics.length && (
                    <div className="productCard-subSection">
                    <h4 className="ui sub header">Informationen</h4>
                    <table className="ui very basic collapsing celled table">
                      <tbody>
                        { info_topics.map( topic =>
                              (<tr key={topic}>
                                <td>
                                  <b>{subjectNames[topic]}</b>
                                </td>
                                <td>
                                  {renderValueString[topic] ? renderValueString[topic](p) : p[topic]}
                                </td>
                              </tr>)
                        )}
                      </tbody>
                    </table>
                    </div>
                  )}
                </div>
              </Card>
    )
  }
}
