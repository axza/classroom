import React from 'react';

import { Button, Icon, Input, Search, Segment } from 'semantic-ui-react'

export default class SearchFilter extends React.Component {
  render() {
    let filterState = this.props.filterState;

    return (
      <Segment compact={!filterState} color={filterState ? 'green' : null}>
      {filterState && <Button onClick={() => this.props.onToggle()} basic icon='close' className="ui right floated button" />}
      <Input
        icon={{ name: 'search' }}
        placeholder='Freitextsuche...'
        onKeyDown={(e) => e.keyCode === 13 && this.props.onToggle(e.target.value)}
        />
      {filterState && <span className="searchFilter-state">Aktuelle Suche: <b>{filterState}</b></span>}
      </Segment>
    )
  }
}

export function searchFilterPredicate(p, filterState) {
  if(filterState) {
    return p.name.toLowerCase().includes(filterState) || p.short_description.toLowerCase().includes(filterState)
  }
}
