#!/usr/bin/env python3

import re
import json
import csv
import argparse

NUM_HEADER_LINES=4

def convert_row(row):
    fresh_row = {
            "name": row.get("Name des Tools\nServices oder der WebApp"),
            "short_description": row.get("Kurzbeschreibung\n(2-5 Sätze)"),
            "category": [s.strip() for s in row.get("Kategorie,\nGruppierung", '').split(',') if len(s.strip())],
            "subject": [s.strip() for s in row.get("Schulfach", '').split(',') if len(s.strip())],
            "class": row.get("geeignet für\nKlassenstufe", ''),
            "audience": [s.strip() for s in row.get("Zielgruppe", '').split(',') if len(s.strip())],
            "cost": 'free' if row.get("Kostenlos").strip() else 'paid',
            "cost_comment": row.get("Kostenkommentar").strip(),
            "clients": [],
            "url": row["Link zur Produkt Webseite"],
            "wiki": row["Link zu Wikipedia Seite\ndes Produkts"],
            "complexity": row["Komplexität\n(1-10)"],
            "asynch": row["Alle gleichzeitig online? \nOder auch offline?"],
            "infrastructure": row["Benötigte \n\"Infrastruktur\"\n(Server)"],
            "test_instance": row["Demo-Instanz"],
            "corona_offer": row["Spezielle Angebote zur Corona Krise"],
            }

    if not fresh_row["name"]:
        return

    for field in ["url", "wiki", "test_instance", "corona_offer"]:
        if re.match(r'^[-\s]+$', fresh_row[field]):
            fresh_row[field] = ''

    url = fresh_row.get('url')
    if url:
        domain = re.sub(r".*//([^\/]*).*", r"\1", url)
        fresh_row['logo'] = "https://logo.clearbit.com/" + domain

    for c in ["iOS", "Android", "WebApp", "Win PC App", "MacOS"]:
        if row.get(c):
            fresh_row["clients"] += [c]

    class_range = fresh_row.get('class', '')
    m = re.match(r'(\d+)-(\d+)', class_range)
    if m:
        fresh_row["class_min"] = int(m.group(1))
        fresh_row["class_max"] = int(m.group(2))
    else:
        m = re.match(r'(\d+)', class_range)
        if m:
            fresh_row["class_min"] = int(m.group(1))
            fresh_row["class_max"] = int(m.group(1))
        elif re.match(r'alle', class_range, re.IGNORECASE):
            fresh_row["class_min"] = 0
            fresh_row["class_max"] = 13
        else:
            fresh_row["class_min"] = -1
            fresh_row["class_max"] = -1


    return fresh_row


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    args = parser.parse_args()

    result_set = []
    with open(args.file) as f:
        for _ in range(NUM_HEADER_LINES):
            next(f)

        reader = csv.DictReader(f, dialect=csv.excel)
        for row in reader:
            row = convert_row(row)
            if row:
                result_set += [row]

    print(json.dumps({ 'results': result_set }, indent=4))
