# Classroom

This tool was created during the German #WirVsVirus hackathon 20-22 March 2020 by the "Virtuelles Klassenzimmer" team (<https://devpost.com/software/1_0026_580_virtuelles-klassenzimmer>)

Don't be too harsh juding the code (there's a LOT of repetitions, dirtyness, and shortcuts...) as it's just a prototype built in about 24h. Though it actually does it's job :)


## Build

`docker build -t classroom .` to build the docker image and
`docker run -p 8080:8080 classroom` to run it


## Dev

`npm install` to install packages, then
`npm run serve:dev` to start local webpack-dev-server


## Data

At the moment we're using `convert_csv.py` to convert the CSV file from google sheets to a JSON file and save it to `/assets/tools.json` - until we've got a proper DB ;)
